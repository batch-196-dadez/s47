console.log('hi');

// Show Posts
const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
};

/*
	fetch method
	Syntax:
		fetch('url', options)
		url- this is the url which the request is to be made
	
*/
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data));

// Add Post
document.querySelector('#form-add-post').addEventListener("submit", (e) => {
	// cancelling the event's default action
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post successfully added');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

// Update Post
document.querySelector('#form-edit-post').addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		/*mini-activity: fill in the appropriate value for updating post
		time: 5 mins
		*/
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Post successfully updated");

		/*mini-activity: 15 mins
			1. all inputs from edit form after submitting should be cleared
			2. search how to set the attribute back again to disabled for the button
			3. (optional) send gif output to batch hangouts 
		*/

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute('disabled', true);
	})
});

/*
	Activity:
		>> Write the necessary code to delete a post
		>> Make a function called deletePost.
		>> Pass a parameter id or follow the typicode route for delete
		>> Use fetch method and the options
		>> (optional) send gif output in Batch Hangouts or send done once you're done
*/
const deletePost = (id) => {

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "DELETE",
		body: JSON.stringify(),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
		alert('Post sucessfully deleted')
		document.querySelector(`#post-${id}`).remove()
	})
	;
}
